package com.athena.model;

import javax.persistence.Entity;

@Entity
public class Aluno extends Usuario {
	private String curso;
	private int anoPrevFormacao;
	private String interesesPequisa;

	//TODO: icnluir as valida��es com anota��es
	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public int getAnoPrevFormacao() {
		return anoPrevFormacao;
	}

	public void setAnoPrevFormacao(int anoPrevFormacao) {
		this.anoPrevFormacao = anoPrevFormacao;
	}

	public String getInteresesPequisa() {
		return interesesPequisa;
	}

	public void setInteresesPequisa(String interesesPequisa) {
		this.interesesPequisa = interesesPequisa;
	}

	@Override
	public String toString() {
		return "Aluno [curso=" + curso + ", anoPrevFormacao=" + anoPrevFormacao + ", interesesPequisa="
				+ interesesPequisa + ", getnUsp()=" + getnUsp() + ", getNome()=" + getNome() + ", getEmail()="
				+ getEmail() + ", getTelefone()=" + getTelefone() + ", getCelular()=" + getCelular()
				+ ", getCurriculoLattes()=" + getCurriculoLattes() + ", getSenha()=" + getSenha() + ", getSexo()="
				+ getSexo() + ", getPapel()=" + getPapel() + ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
