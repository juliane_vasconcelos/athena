package com.athena.facade;

import com.athena.model.Usuario;

public interface Cadastro {

	public Usuario fazerLogin(String login, String senha);

}
