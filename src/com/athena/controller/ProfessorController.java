package com.athena.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.athena.model.IniciacaoCientifica;
import com.athena.model.Professor;

@Controller
public class ProfessorController {

	@RequestMapping("/editar-perfil-professor")
	public String editarPerfil() {
		System.out.println("Acessando o pagina de edi��o de perfil do professor");
		// TODO: buscar no banco todas as informa��es do professor
		return "professor/edicao/editar-perfil";
	}

	@RequestMapping("editarPerfil")
	public String editarPerfil(Professor professor) {
		System.out.println("Fazendo edicao de perfil do " + professor.toString());

		try {
			// TODO: fazer o merge dos dados no banco
		} catch (Exception e) {
			e.printStackTrace();
			return "professor/edicao/edicao-perfil-erro";
		}
		return "professor/edicao/edicao-perfil-sucesso";
	}

	@RequestMapping("/nova-iniciacao")
	public String cadastrarIniciacao() {
		System.out.println("Acessando o pagina de cadastro de inicia��o cient�ficas");
		return "professor/iniciacao/cadastro/cadastroIC";
	}

	@RequestMapping("cadastrarIniciacao")
	public String cadastrarIniciacao(IniciacaoCientifica iniciacaoCientifica) {
		System.out.println("Fazendo cadastro de inicia��o " + iniciacaoCientifica.toString());

		try {
			// TODO: preencher o email do professor com o usuario logado
			// TODO: fazer o cadastro da inicia��o no banco de dados
		} catch (Exception e) {
			e.printStackTrace();
			return "professor/iniciacao/cadastro/iniciacao-cadastro-erro";
		}
		return "professor/iniciacao/cadastro/iniciacao-cadastro-sucesso";
	}

	@RequestMapping("/listar-iniciacao-professor")
	public String listarIniciacao() {
		System.out.println("Acessando o pagina de listagem de todas as inicia��es cient�ficas deste professor");
		// TODO: buscar no banco todas as inicia��es e imprimir na tela
		// TODO: exibir na tela campos de remover, editar (caso professor) e de
		// inscrever (caso aluno)
		return "professor/iniciacao/ver-todas-iniciacoes";
	}

}