package com.athena.services;

import java.util.List;

import com.athena.model.Curso;
import com.athena.model.IniciacaoCientifica;
import com.athena.model.Usuario;

public interface IniciacaoService {

	public boolean cadastrarIniciacao(IniciacaoCientifica iniciacaoCientifica);

	public boolean editarIniciacao(IniciacaoCientifica iniciacaoCientifica);

	public boolean removerIniciacao(IniciacaoCientifica iniciacaoCientifica);

	public IniciacaoCientifica buscarIniciacaoPorNome(String nome);

	public IniciacaoCientifica buscarIniciacaoPorCodigo(int codIC);

	public IniciacaoCientifica buscarIniciacaoPorCursos(List<Curso> cursos);

	public IniciacaoCientifica buscarIniciacaoPorUsuario(Usuario usuario);

	public List<IniciacaoCientifica> buscarTodasIniciacoes();

}
